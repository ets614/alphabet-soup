package com.enlighten.alphabet.soup;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import org.junit.Assert;
import org.junit.Test;

public class WordSearchKeyGeneratorTest {

    @Test
    public void test1() throws FileNotFoundException {
        Assert.assertEquals(readExpected("src/test/resources/out1"), WordSearchKeyGenerator.generateAnswerKey("src/test/resources/test1"));
    }

    @Test
    public void test2() throws FileNotFoundException {
        Assert.assertEquals(readExpected("src/test/resources/out2"), WordSearchKeyGenerator.generateAnswerKey("src/test/resources/test2"));
    }

    @Test
    public void test3() throws FileNotFoundException {
        Assert.assertEquals(readExpected("src/test/resources/out3"), WordSearchKeyGenerator.generateAnswerKey("src/test/resources/test3"));
    }

    @Test
    public void test4() throws FileNotFoundException {
        Assert.assertEquals(readExpected("src/test/resources/out4"), WordSearchKeyGenerator.generateAnswerKey("src/test/resources/test4"));
    }

    private static String readExpected(final String filePath) throws FileNotFoundException {
        final StringBuilder expectedSb = new StringBuilder();
        final Scanner sc = new Scanner(new File(filePath));
        while (sc.hasNextLine()) {
            if (expectedSb.length() != 0) {
                expectedSb.append("\n");
            }

            expectedSb.append(sc.nextLine());
        }
        sc.close();
        return expectedSb.toString();
    }
}
