package com.enlighten.alphabet.soup;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Logger;

/**
 * Locates each word in a word search to produce the answer key.
 * 
 * @author Evan Subar
 *
 */
public class WordSearchKeyGenerator {
    private static final Logger LOGGER = Logger.getLogger(WordSearchKeyGenerator.class.getName());

    private static final String DIMENSIONS_SPLIT_STR = "x";

    /**
     * Find each word in the word search.
     * 
     * @param inputFilePath
     *            file path to the word search
     * @return the location of each word in the word search
     * @throws FileNotFoundException
     *             if the file does not exist
     */
    public static String generateAnswerKey(final String inputFilePath) throws FileNotFoundException {
        LOGGER.info("Reading file " + inputFilePath);
        final File inputFile = new File(inputFilePath);
        final Scanner sc = new Scanner(inputFile);

        // DIMENSIONS
        final String dimensionsStr = sc.nextLine();
        final String[] dimensionsStrParts = dimensionsStr.split(DIMENSIONS_SPLIT_STR);
        final int rows = Integer.parseInt(dimensionsStrParts[0]);
        final int cols = Integer.parseInt(dimensionsStrParts[1]);
        LOGGER.info("There are " + rows + " rows and " + cols + " columns in the word search.");

        // WORD SEARCH
        final char[][] wordSearch = new char[rows][cols];
        final Map<Character, List<Location>> letterToLocations = new HashMap<>(); // an
        // index
        for (int row = 0; row < rows; row++) {
            final String[] lineParts = sc.nextLine().split(" ");
            for (int col = 0; col < cols; col++) {
                final char letter = lineParts[col].charAt(0);
                wordSearch[row][col] = letter;

                // Register that letter is located at row/col.
                List<Location> locations = letterToLocations.get(letter);
                if (locations == null) {
                    locations = new ArrayList<>();
                    letterToLocations.put(letter, locations);
                }
                locations.add(new Location(row, col));
            }
        }
        printWordSearch(wordSearch);

        // WORD BANK
        final Map<String, Location> wordToLocation = new LinkedHashMap<>();
        while (sc.hasNextLine()) {
            wordToLocation.put(sc.nextLine(), null);
        }
        sc.close();

        // SEARCH
        for (final Map.Entry<String, Location> entry : wordToLocation.entrySet()) { // for each word in the word bank

            // Locate the word.
            final String trimmedWord = entry.getKey().replace(" ", ""); // trim the word to handle words that have spaces
            final char firstLetter = trimmedWord.charAt(0);
            for (final Location firstLetterLocation : letterToLocations.get(firstLetter)) { // for each location of the 1st letter
                final Location wordLocation = new Location(firstLetterLocation.startRow, firstLetterLocation.startCol);
                findWord(wordSearch, trimmedWord, 0, wordLocation.startRow, wordLocation.startCol, wordLocation, null);

                if (wordLocation.endRow == null && wordLocation.endCol == null) { // word not found
                    continue; // try a different starting location for the 1st letter
                }

                // Otherwise, found the word! Move on to finding the next word.
                wordToLocation.put(entry.getKey(), wordLocation);
                break;
            }
        }

        // RESULT
        final StringBuilder resultSb = new StringBuilder();
        for (final Map.Entry<String, Location> entry : wordToLocation.entrySet()) {
            if (resultSb.length() != 0) {
                resultSb.append("\n");
            }

            resultSb.append(entry.getKey());
            final Location loc = entry.getValue();
            if (loc != null) {
                resultSb.append(" ").append(loc);
            }
        }
        final String result = resultSb.toString();
        LOGGER.info("Result!\n" + result);
        return result;
    }

    /**
     * Recursive method for finding each word's location.
     * 
     * @param wordSearch
     *            the word search
     * @param word
     *            the word to locate
     * @param wordIndex
     *            index of the current letter in the word to find
     * @param row
     *            the row to check
     * @param col
     *            the column to check
     * @param location
     *            the result (i.e. the location of the word in the word search)
     * @param direction
     *            the current direction being searched (to search in a straight
     *            line)
     */
    private static void findWord(char[][] wordSearch, String word, int wordIndex, int row, int col, Location location, SearchDirection direction) {
        if (location.endCol != null) {
            return; // already found the word's location; nothing else to do
        }

        if (row < 0 || col < 0 || row >= wordSearch.length || col >= wordSearch[0].length) { // out of bounds
            return;
        }

        if (wordSearch[row][col] != word.charAt(wordIndex)) { // letter doesn't match
            return; // go back
        }

        // Otherwise, letter does match!

        if (word.length() == wordIndex + 1) { // reached the end of the word
            // Return the end location.
            location.endCol = col;
            location.endRow = row;
            return;
        }

        if (direction != null) {
            // Continue in the same direction
            switch (direction) {
                case LEFT:
                    findWord(wordSearch, word, wordIndex + 1, row, col - 1, location, SearchDirection.LEFT);
                    break;
                case RIGHT:
                    findWord(wordSearch, word, wordIndex + 1, row, col + 1, location, SearchDirection.RIGHT);
                    break;
                case DOWN:
                    findWord(wordSearch, word, wordIndex + 1, row + 1, col, location, SearchDirection.DOWN);
                    break;
                case UP:
                    findWord(wordSearch, word, wordIndex + 1, row - 1, col, location, SearchDirection.UP);
                    break;
                case BOTTOM_LEFT:
                    findWord(wordSearch, word, wordIndex + 1, row + 1, col - 1, location, SearchDirection.BOTTOM_LEFT);
                    break;
                case BOTTOM_RIGHT:
                    findWord(wordSearch, word, wordIndex + 1, row + 1, col + 1, location, SearchDirection.BOTTOM_RIGHT);
                    break;
                case TOP_LEFT:
                    findWord(wordSearch, word, wordIndex + 1, row - 1, col - 1, location, SearchDirection.TOP_LEFT);
                    break;
                case TOP_RIGHT:
                    findWord(wordSearch, word, wordIndex + 1, row - 1, col + 1, location, SearchDirection.TOP_RIGHT);
                    break;
            }
        } else {
            // Otherwise, determine which direction to go.
            findWord(wordSearch, word, wordIndex + 1, row, col - 1, location, SearchDirection.LEFT);
            findWord(wordSearch, word, wordIndex + 1, row, col + 1, location, SearchDirection.RIGHT);
            findWord(wordSearch, word, wordIndex + 1, row + 1, col, location, SearchDirection.DOWN);
            findWord(wordSearch, word, wordIndex + 1, row - 1, col, location, SearchDirection.UP);
            findWord(wordSearch, word, wordIndex + 1, row + 1, col - 1, location, SearchDirection.BOTTOM_LEFT);
            findWord(wordSearch, word, wordIndex + 1, row + 1, col + 1, location, SearchDirection.BOTTOM_RIGHT);
            findWord(wordSearch, word, wordIndex + 1, row - 1, col - 1, location, SearchDirection.TOP_LEFT);
            findWord(wordSearch, word, wordIndex + 1, row - 1, col + 1, location, SearchDirection.TOP_RIGHT);
        }
    }

    /**
     * Direction being explored in the word search.
     */
    private enum SearchDirection {
        LEFT, RIGHT, UP, DOWN, TOP_LEFT, TOP_RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT;
    }

    /**
     * Coordinates in the word search grid.
     */
    private static class Location {
        private static final String LOC_SEPARATOR = ":";

        private Integer startRow;
        private Integer startCol;
        private Integer endRow;
        private Integer endCol;

        private Location(int startRow, int startCol) {
            this.startRow = startRow;
            this.startCol = startCol;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(startRow).append(LOC_SEPARATOR).append(startCol);
            sb.append(" ");
            sb.append(endRow).append(LOC_SEPARATOR).append(endCol);
            return sb.toString();
        }
    }

    /**
     * Util method for displaying the word search in the console.
     * 
     * @param wordSearch
     *            the word search to display
     */
    private static void printWordSearch(final char[][] wordSearch) {
        final StringBuilder result = new StringBuilder("Word Search:").append("\n");
        for (int row = 0; row < wordSearch.length; row++) {
            for (int col = 0; col < wordSearch[row].length; col++) {
                if (col != 0) {
                    result.append(" ");
                }
                result.append(wordSearch[row][col]);
            }
            result.append("\n");
        }
        LOGGER.info(result.toString());
    }
}
